<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'title', 'slug', 'content'
    ];

    //one to many
    public function user(){
    	return $this->belongsTo('App\User');
    }

    //one to many tips another way try
    public function author(){
    	return $this->belongsTo('App\User', 'user_id');
    }

}
